#include "MsiCells.h"

MsiCell& MsiCells::operator[](unsigned int pIndex)
{
	if (pIndex > mCells.size())
		MsiUtilities::CriticalError(L"Trying to access an invalid column from the row!");

	return mCells[pIndex - 1]; //the MSI cells start from 1
}

MsiCell MsiCells::operator[](unsigned int pIndex) const
{
	if (pIndex > mCells.size())
		MsiUtilities::CriticalError(L"Trying to access an invalid column from the row!");

	return mCells[pIndex - 1]; //the MSI cells start from 1
}

unsigned int MsiCells::Size() const
{
	return mCells.size();
}