#pragma once
#include "MsiCell.h"
#include <vector>

/** @file */

/**
*Represents a collections of cells.
*
*This class is a wrapper over a std::vector of MsiCells. It can represent a row in
*the Msi Database or any other group of cells (like a primary key)
*
*@see MsiCell
*/
class MSI_EXPORT MsiCells
{
public:
	/**
	*Class constructor.
	*
	*Default constructor used in declarations without initialization.
	*
	*@see MsiCells(MsiCell pCell, Args&&... pCells)
	*/
	MsiCells() {}

	/**
	*Class constructor.
	*
	*It receives a variable number of MsiCell. Because MsiCell has constructor overloads
	*for int, std::wstring and std::vector<char>, the arguments of this types construct automatically MsiCell.
	*
	*@code
	*	MsiCells(10, L"abc", std::vector<char>('1, 0x38, '-'), 23);
	*@endcode
	*
	*@see MsiCell
	*
	*@remark First argument is not an universal reference so it doesn't overwrite the copy and move constructors.
	*/
	template<typename... Args>
	MsiCells(MsiCell pCell, Args&&... pCells);

	/**
	*Adds cells to an existing MsiCells collection.
	*
	*It receives a variable number of MsiCell. Because MsiCell has constructor overloads
	*for int, std::wstring and std::vector<char>, the arguments of this types construct automatically MsiCell.
	*
	*@see MsiCells(MsiCell pCell, Args&&... pCells)
	*/
	template<typename T, typename... Args>
	void AddCells(T&& pCell, Args&&... pCells);

	/**
	*Operator overloading to easily access or modify each MsiCell in the collection.
	*
	*@param pIndex The index of the requested element. Starts from 1.
	*@return It returns a MsiCell by reference, so it can be modified.
	*
	*@see operator[](unsigned int pIndex) const
	*
	*@remark The index is one based!
	*/
	MsiCell& operator[](unsigned int pIndex);

	/**
	*Operator overloading for easier access to each MsiCell in the collection.
	*
	*@param pIndex The index of the requested element. Starts from 1.
	*@return It returns a MsiCell by value.
	*
	*@see operator[](unsigned int pIndex)
	*
	*@remark The index is one based!
	*/
	MsiCell operator[](unsigned int pIndex) const;

	/**
	*Getter for the size of this collection of MsiCell objects.
	*@return It returns the number of MsiCell objects in this MsiCells collection.
	*/
	unsigned int Size() const;

private:
	//Needed by the templated AddCells for the recursive call.
	void AddCells() {};

	std::vector<MsiCell> mCells;
};

/**
*Although a row is just a collection of cells, naming it MsiRow better represents how it's used.
*/
using MsiRow = MsiCells;

#include "MsiCells.hpp"