template<typename... Args>
MsiMeta::MsiMeta(MsiColInfo pColInfo, Args&&... pColInfos)
{
	AddColInfo(std::move(pColInfo), std::forward<Args>(pColInfos)...);
}

template<typename T, typename... Args>
void MsiMeta::AddColInfo(T&& pColInfo, Args&&... pColInfos)
{
	mColsInfo.emplace_back(pColInfo);
	AddColInfo(std::forward<Args>(pColInfos)...);
}