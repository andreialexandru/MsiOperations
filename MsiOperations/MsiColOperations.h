#pragma once
#include "MsiTable.h"
#include <string>

/**@file*/

/**
*Operand for SQL operations.
*
*Class that contains a column name in format TableName.ColumnName and the column's type.
*Used as an operand by the overloaded operators in order to construct an  MsiColOperations object.
*@see MsiColOperations
*/
class MsiColOp
{
public:
	/**
	*Class Constructor
	*
	*Constructs an operand from a table and a column name.
	*@param pTable The table that contains the column.
	*@param pCol The column name.
	*/
	MsiColOp(const MsiTable& pTable, const std::wstring& pCol)
		: mColCompleteName(pTable.GetName() + L"." + pCol), mColType(pTable.GetMeta()[pCol].Type())
	{
	}

	/**
	*Class Constructor
	*
	*Constructs an operand from a string. Used for comparing a column with a string (Table.Col <> "abc")
	*@param pStr The string used to construct an operand.
	*/
	MsiColOp(const std::wstring& pStr)
		: mColCompleteName(L"'" + pStr + L"'"), mColType(MsiDataType::STRING)
	{
	}

	/**
	*Class Constructor
	*
	*Constructs an operand from a string. Used for comparing a column with a string (Table.Col <> "abc")
	*@param pStr The string used to construct an operand.
	*/
	MsiColOp(const wchar_t* pStr)
		: MsiColOp(std::wstring(pStr))
	{
	}

	/**
	*Class Constructor
	*
	*Constructs an operand from an integer. Used for comparing a column with a int (Table.Col >= 10)
	*@param pInt The integer used to construct an operand.
	*/
	MsiColOp(int pInt)
		: mColCompleteName(std::to_wstring(pInt)), mColType(MsiDataType::INTEGER)
	{
	}

	/**
	*Getter for the column's type.
	*/
	MsiDataType GetType() const { return mColType; }

	/**
	*Getter for the column's complete name (TableName.ColumnName).
	*/
	const std::wstring& GetStr() const { return mColCompleteName; }

private:
	std::wstring mColCompleteName;
	MsiDataType mColType;
};

/**
*Class that accumulates SQL operations.
*
*Class that contains a string which accumulates the SQL operations.
*@code
*	Table1.Col1 = Table2.Col2 AND Table2.Col1 IS NULL
*@endcode
*@see MsiColOp
*/
class MsiColOperations
{
public:

	/**
	*Class Constructor
	*
	*It takes a string and surrounds it by parenthesis in order to preserve the operations order.
	*@param pStr String to be surrounded by parenthesis and saved.
	*/
	MsiColOperations(const std::wstring& pStr)
	{
		mExpression = L"(" + pStr + L")";
	}

	/**
	*Class Copy Constructor
	*
	*It takes a another MsiColOperations object and surrounds it's internal string
	*by parenthesis in order to preserve the operations order.
	*@param pOther MsiColOperations object from which the string is taken  
	*to be surrounded by parenthesis and saved.
	*/
	MsiColOperations(const MsiColOperations& pOther)
	{
		mExpression = L"(" + pOther.mExpression + L")";
	}

	/**
	*Class Move Constructor
	*
	*It takes a another MsiColOperations object and surrounds it's internal string
	*by parenthesis in order to preserve the operations order.
	*@param pOther MsiColOperations object from which the string is taken
	*to be surrounded by parenthesis and saved.
	*/
	MsiColOperations(MsiColOperations&& pOther)
	{
		mExpression = L"(" + pOther.mExpression + L")";
	}

	/**
	*Getter for the internal string that represents an SQL operation.
	*/
	std::wstring GetStr() const { return mExpression; }

private:
	std::wstring mExpression;
};

/**
*Operator overload that generates a string in SQL format.
*
*Code Generated:
*@code
*	TableName.ColName < TableName2.ColName2
*@endcode
*@param l Left hand side argument.
*@param r Right hand side argument.
*@see MsiColOperations
*@see MsiColOp
*/
inline MsiColOperations operator<(const MsiColOp& l, const MsiColOp& r)
{
	if (l.GetType() == r.GetType() && l.GetType() == MsiDataType::INTEGER)
		return MsiColOperations(l.GetStr() + L" < " + r.GetStr());
	
	MsiUtilities::CriticalError(L"Cannot use operator< on columns with a type different than INTEGER!");
	return MsiColOperations(L"");
}

/**
*Operator overload that generates a string in SQL format.
*
*Code Generated:
*@code
*	TableName.ColName > TableName2.ColName2
*@endcode
*@param l Left hand side argument.
*@param r Right hand side argument.
*@see MsiColOperations
*@see MsiColOp
*/
inline MsiColOperations operator>(const MsiColOp& l, const MsiColOp& r)
{
	if (l.GetType() == r.GetType() && l.GetType() == MsiDataType::INTEGER)
		return MsiColOperations(l.GetStr() + L" > " + r.GetStr());
	
	MsiUtilities::CriticalError(L"Cannot use operator> on columns with a type different than INTEGER!");
	return MsiColOperations(L"");
}

/**
*Operator overload that generates a string in SQL format.
*
*Code Generated:
*@code
*	TableName.ColName <= TableName2.ColName2
*@endcode
*@param l Left hand side argument.
*@param r Right hand side argument.
*@see MsiColOperations
*@see MsiColOp
*/
inline MsiColOperations operator<=(const MsiColOp& l, const MsiColOp& r)
{
	if (l.GetType() == r.GetType() && l.GetType() == MsiDataType::INTEGER)
		return MsiColOperations(l.GetStr() + L" <= " + r.GetStr());

	MsiUtilities::CriticalError(L"Cannot use operator<= on columns with a type different than INTEGER!");
	return MsiColOperations(L"");
}

/**
*Operator overload that generates a string in SQL format.
*
*Code Generated:
*@code
*	TableName.ColName >= TableName2.ColName2
*@endcode
*@param l Left hand side argument.
*@param r Right hand side argument.
*@see MsiColOperations
*@see MsiColOp
*/
inline MsiColOperations operator>=(const MsiColOp& l, const MsiColOp& r)
{
	if (l.GetType() == r.GetType() && l.GetType() == MsiDataType::INTEGER)
		return MsiColOperations(l.GetStr() + L" >= " + r.GetStr());

	MsiUtilities::CriticalError(L"Cannot use operator>= on columns with a type different than INTEGER!");
	return MsiColOperations(L"");
}


/**
*Operator overload that generates a string in SQL format.
*
*Code Generated:
*@code
*	TableName.ColName = TableName2.ColName2
*@endcode
*@param l Left hand side argument.
*@param r Right hand side argument.
*@see MsiColOperations
*@see MsiColOp
*/
inline MsiColOperations operator==(const MsiColOp& l, const MsiColOp& r)
{
	if (l.GetType() == r.GetType() && (l.GetType() == MsiDataType::INTEGER || l.GetType() == MsiDataType::STRING))
		return MsiColOperations(l.GetStr() + L" = " + r.GetStr());

	MsiUtilities::CriticalError(L"Cannot use operator== on columns with a type different than INTEGER or STRING!");
	return MsiColOperations(L"");
}

/**
*Operator overload that generates a string in SQL format.
*
*Code Generated:
*@code
*	TableName.ColName <> TableName2.ColName2
*@endcode
*@param l Left hand side argument.
*@param r Right hand side argument.
*@see MsiColOperations
*@see MsiColOp
*/
inline MsiColOperations operator!=(const MsiColOp& l, const MsiColOp& r)
{
	if (l.GetType() == r.GetType() && (l.GetType() == MsiDataType::INTEGER || l.GetType() == MsiDataType::STRING))
		return MsiColOperations(l.GetStr() + L" <> " + r.GetStr());

	MsiUtilities::CriticalError(L"Cannot use operator<> on columns with a type different than INTEGER or STRING!");
	return MsiColOperations(L"");
}


/**
*Function that generates a string in SQL format.
*
*Code Generated:
*@code
*	TableName.ColName is null
*@endcode
*@param pColInfo MsiColOp operand that is used to generate an SQL string.
*@see MsiColOperations
*@see MsiColOp
*/
inline MsiColOperations IsNull(const MsiColOp& pColInfo)
{
	if (pColInfo.GetType() == MsiDataType::NONE)
	{
		MsiUtilities::CriticalError(L"Cannot use IsNull on columns with type NONE!");
		return MsiColOperations(L"");
	}

	return MsiColOperations(pColInfo.GetStr() + L" is null");
}

/**
*Function that generates a string in SQL format.
*
*Code Generated:
*@code
*	TableName.ColName is not null
*@endcode
*@param pColInfo MsiColOp operand that is used to generate an SQL string.
*@see MsiColOperations
*@see MsiColOp
*/
inline MsiColOperations IsNotNull(const MsiColOp& pColInfo)
{
	if (pColInfo.GetType() == MsiDataType::NONE)
	{
		MsiUtilities::CriticalError(L"Cannot use IsNotNull on columns with type NONE!");
		return MsiColOperations(L"");
	}

	return MsiColOperations(pColInfo.GetStr() + L" is not null");
}

/**
*Operator overload that generates a string in SQL format.
*
*Code Generated:
*@code
*	expr1 AND expr2
*@endcode
*@param l Left hand side argument.
*@param r Right hand side argument.
*@see MsiColOperations
*@see MsiColOp
*/
inline MsiColOperations operator&&(const MsiColOperations& l, const MsiColOperations& r)
{
	return MsiColOperations(l.GetStr() + L" AND " + r.GetStr());
}

/**
*Operator overload that generates a string in SQL format.
*
*Code Generated:
*@code
*	expr1 OR expr2
*@endcode
*@param l Left hand side argument.
*@param r Right hand side argument.
*@see MsiColOperations
*@see MsiColOp
*/
inline MsiColOperations operator||(const MsiColOperations& l, const MsiColOperations& r)
{
	return MsiColOperations(l.GetStr() + L" OR " + r.GetStr());
}