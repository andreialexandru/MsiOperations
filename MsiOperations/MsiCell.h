#pragma once
#include "MsiUtilities.h"
#include <vector>
#include <string>

/** @file */

/**
*Represents a cell in the MSI database.
*
*This class holds either an integer, a string or a stream (vector<chars>) and
*it provides helper functions for getting and setting this values. It also can convert
*to the internal type.
*/
class MSI_EXPORT MsiCell
{
public:
	/**
	*Class constructor.
	*
	*Default constructor that initialize the internal MsiDataType to MsiDataType::NONE.
	*
	*@see MsiCell(int pIntVal)
	*@see MsiCell(const std::vector<char>& pStreamVal)
	*@see MsiCell(const std::wstring& pStringVal)
	*@see MsiCell(const wchar_t* pStringVal)
	*@see MsiCell(const MsiCell& pOther)
	*/
	MsiCell();

	/**
	*Class constructor.
	*
	*This constructor is used to construct a cell containing an int value.
	*@param pIntVal The integer value to save in the cell.
	*
	*@see MsiCell()
	*@see MsiCell(const std::vector<char>& pStreamVal)
	*@see MsiCell(const std::wstring& pStringVal)
	*@see MsiCell(const wchar_t* pStringVal)
	*@see MsiCell(const MsiCell& pOther)
	*/
	MsiCell(int pIntVal);

	/**
	*Class constructor.
	*
	*This constructor is used to construct a cell containing a stream (a vector of chars).
	*@param pStreamVal The stream to save in the cell.
	*
	*@see MsiCell()
	*@see MsiCell(int pIntVal)
	*@see MsiCell(const std::wstring& pStringVal)
	*@see MsiCell(const wchar_t* pStringVal)
	*@see MsiCell(const MsiCell& pOther)
	*/
	MsiCell(const std::vector<char>& pStreamVal);

	/**
	*Class constructor.
	*
	*This constructor is used to construct a cell containing a string.
	*@param pStringVal The string to save in the cell.
	*
	*@see MsiCell()
	*@see MsiCell(int pIntVal)
	*@see MsiCell(const std::vector<char>& pStreamVal)
	*@see MsiCell(const wchar_t* pStringVal)
	*@see MsiCell(const MsiCell& pOther)
	*/
	MsiCell(const std::wstring& pStringVal);

	/**
	*Class constructor.
	*
	*This constructor is used to construct a cell containing a string. Used so we can send
	*a string literal.
	*@param pStringVal The string to save in the cell.
	*
	*@see MsiCell()
	*@see MsiCell(int pIntVal)
	*@see MsiCell(const std::vector<char>& pStreamVal)
	*@see MsiCell(const std::wstring& pStringVal)
	*@see MsiCell(const MsiCell& pOther)
	*/
	MsiCell(const wchar_t* pStringVal);

	/**
	*Copy constructor.
	*
	*@param pOther The object we want to copy.
	*/
	MsiCell(const MsiCell& pOther);

	/**
	*Copy assignment operator.
	*
	*@param pOther The object we want to copy.
	*/
	MsiCell& operator=(const MsiCell& pOther);

	/**
	*Move constructor.
	*
	*@param pOther The object we want to move into this.
	*/
	MsiCell(MsiCell&& pOther);
	
	/**
	*Move assignment operator.
	*
	*@param pOther The object we want to move into this.
	*/
	MsiCell& operator=(MsiCell&& pOther);

	/**
	*Class destructor.
	*/
	~MsiCell();

	/**
	*Overload for the assignment operator to support integers.
	*
	*Used to set more easily values to the cell.
	*@param pIntVal The integer value to save in the cell.
	*/
	MsiCell& operator=(int pIntVal);

	/**
	*Overload for the assignment operator to support streams.
	*
	*Used to set more easly values to the cell.
	*@param pStreamVal The stream value to save in the cell.
	*/
	MsiCell& operator=(const std::vector<char>& pStreamVal);
	
	/**
	*Overload for the assignment operator to support strings.
	*
	*Used to set more easily values to the cell.
	*@param pStringVal The string value to save in the cell.
	*/
	MsiCell& operator=(const std::wstring& pStringVal);

	/**
	*Getter for retrieving the internal integer value.
	*
	*Used to retrieve the internal value. It returns a Maybe containing a value,
	*if the Cell's internal value is an int, or an Exception otherwise.
	*@return Is a Maybe<int> that may be the internal value or an Exception.
	*@see Maybe
	*/
	Maybe<int> Int() const;

	/**
	*Getter for retrieving the internal stream value.
	*
	*Used to retrieve the internal value. It returns a Maybe containing a value,
	*if the Cell's internal value is a stream, or an Exception otherwise.
	*@return Is a Maybe<std::vector<char>> that may be the internal value or an Exception.
	*@see Maybe
	*/
	Maybe<std::vector<char>> Stream() const;

	/**
	*Getter for retrieving the internal string value.
	*
	*Used to retrieve the internal value. It returns a Maybe containing a value,
	*if the Cell's internal value is a string, or an Exception otherwise.
	*@return Is a Maybe<std::wstring> that may be the internal value or an Exception.
	*@see Maybe
	*/
	Maybe<std::wstring> String() const;

	/**
	*Getter for retrieving the type of the internal value.
	*
	*Returns an enum for identifying what type has the internal value.
	*@retval MsiDataType::NONE No value is set in the cell.
	*@retval MsiDataType::INTEGER The internal value is an int.
	*@retval MsiDataType::STREAM The internal value is a stream (std::vector<char>)
	*@retval MsiDataType::STRING The internal value is a string (std::wstring)
	*
	*@see MsiDataType
	*/
	MsiDataType Type() const;

	/**
	*Overload for the equality operator.
	*
	*Check if two MsiCell objects are equal - same MsiDataType and same value.
	*@param pOther The MsiCell that is compared with this.
	*/
	bool operator==(const MsiCell& pOther);

	/**
	*Overload for the inequality operator.
	*
	*Check if two MsiCell objects are not equal - different MsiDataType or different value.
	*@param pOther The MsiCell that is compared with this.
	*/
	bool operator!=(const MsiCell& pOther);

	/**
	*Constant representing a null integer in the Msi database.
	*/
	static const int NullInteger;

	/**
	*Constant representing a null stream in the Msi database.
	*/
	static const std::vector<char> NullStream;

	/**
	*Constant representing a null string in the Msi database.
	*/
	static const std::wstring NullString;
	
private:
	void DeleteVal();

	MsiDataType mType;
#pragma warning(disable : 4201) //nonstandard extension used: nameless struct/union

	//This version of visual studio does not support complex types in unions
	union
	{
		int mIntVal;
		struct
		{
			char* mStreamVal;
			unsigned int mStreamLen;
		};
		wchar_t* mStringVal;
	};

#pragma warning(default : 4201)

};
