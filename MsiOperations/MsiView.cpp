#include "MsiView.h"
#include "MsiUtilities.h"
#include "Maybe.h"
#include "MsiCommonFilters.h"
#include <MsiQuery.h>
#include <cwctype>

MsiView::MsiView(const MsiDB& pDb, const MsiQuery& pQuery)
	: mEndOfViewFlag(false), mCurrentRowH(NULL), mCurrPos(0), mViewHandle(NULL)
{
	Execute(pDb, pQuery);

	FillMeta();
}

MsiView::MsiView(MsiView&& pOther)
	: mMeta(std::move(pOther.mMeta)), mCurrentRow(std::move(pOther.mCurrentRow)), mEndOfViewFlag(std::move(pOther.mEndOfViewFlag)),
	mCurrPos(std::move(pOther.mCurrPos))
{
	mViewHandle = pOther.mViewHandle;
	pOther.mViewHandle = NULL;

	mCurrentRowH = pOther.mCurrentRowH;
	pOther.mCurrentRowH = NULL;
}

MsiView& MsiView::operator = (MsiView&& pOther)
{
	if (this != &pOther)
	{
		mMeta = std::move(pOther.mMeta);
		mCurrentRow = std::move(pOther.mCurrentRow);
		mEndOfViewFlag = std::move(pOther.mEndOfViewFlag);
		mCurrPos = std::move(pOther.mCurrPos);

		CloseHandles();

		mViewHandle = pOther.mViewHandle;
		pOther.mViewHandle = NULL;

		mCurrentRowH = pOther.mCurrentRowH;
		pOther.mCurrentRowH = NULL;
	}

	return *this;
}

MsiView::~MsiView()
{
	CloseHandles();
}

const MsiMeta& MsiView::GetMeta() const
{
	return mMeta;
}

bool MsiView::EndOfView() const
{
	return mEndOfViewFlag;
}

bool MsiView::Fetch(MsiFetchFilters pFilters)
{
	if (mCurrentRowH)
		MsiUtilities::Verify(MsiCloseHandle(mCurrentRowH));

	auto res = MsiViewFetch(mViewHandle, &mCurrentRowH);
	if (res == ERROR_SUCCESS)
	{
		//use a tempRow because filling it might fail, so we prevent corrupting the current row
		MsiRow tempRow;
		//fill the row
		try
		{
			for (unsigned int index = 1; index <= mMeta.ColCount(); index++)
			{
				switch (mMeta[index].Type())
				{
				case MsiDataType::INTEGER:
					tempRow.AddCells(MsiRecordGetInteger(mCurrentRowH, index));
					break;
				case MsiDataType::STRING:
					//might trow
					tempRow.AddCells(GetRecordString(mCurrentRowH, index).Get());
					break;
				case MsiDataType::STREAM:
					//might trow
					tempRow.AddCells(GetRecordStream(mCurrentRowH, index).Get());
					break;
				}
			}
		}
		catch (Exception e)
		{
			MsiUtilities::CriticalError(e.Str());

			return false;
		}

		//Check if the fetched row passes the filters. If it fails just one filter, it steps to the next element.
		for (const auto& filter : pFilters.GetFilters())
		{
			if (!filter(mMeta, tempRow))
			{
				return Fetch(std::move(pFilters));
			}
		}

		mCurrPos++;
		mCurrentRow = tempRow;

		return true;
	}
	else if (res == ERROR_NO_MORE_ITEMS)
	{
		mEndOfViewFlag = true;

		return false;
	}
	else
	{
		MsiUtilities::CriticalError(L"MsiFetch failed with error " + std::to_wstring(res));
		
		return false;
	}
}

void MsiView::Reset()
{
	mEndOfViewFlag = false;
	mCurrPos = 0;
	MsiUtilities::Verify(MsiViewExecute(mViewHandle, NULL));
}

const MsiRow& MsiView::GetCurrentRow() const
{
	return mCurrentRow;
}

void MsiView::InsertRow(MsiRow pRow, bool pTemp)
{
	if (IsValid(pRow))
	{
		MSIHANDLE recH = MsiCreateRecord(mMeta.ColCount());
		if (recH)
		{
			if (mCurrentRowH)
				MsiUtilities::Verify(MsiCloseHandle(mCurrentRowH));
			mCurrentRowH = recH;
			mCurrentRow = std::move(pRow);
			UpdateRow();
			if (pTemp)
				MsiUtilities::Verify(MsiViewModify(mViewHandle, MSIMODIFY_INSERT_TEMPORARY, mCurrentRowH));
			else
				MsiUtilities::Verify(MsiViewModify(mViewHandle, MSIMODIFY_INSERT, mCurrentRowH));
		}
		else
		{
			MsiUtilities::LogError(L"Record creation failed.");
		}
	}
	else
	{
		MsiUtilities::LogError(L"The row provided does not have a valid format.");
	}	
}

void MsiView::UpdateCurrRow(MsiRow pRow, bool pCreate)
{
	if (IsValid(pRow))
	{
		//if pCreate and the current handle is not valid, create a new record
		if (pCreate && !mCurrentRowH)
		{
			MSIHANDLE recH = MsiCreateRecord(mMeta.ColCount());
			if (recH)
			{
				mCurrentRowH = recH;
			}
			else
			{
				MsiUtilities::LogError(L"Record creation failed.");
			}
		}

		mCurrentRow = std::move(pRow);
		UpdateRow();
		//if pCreate, create the node if it doesn't exists
		if (pCreate)
			MsiUtilities::Verify(MsiViewModify(mViewHandle, MSIMODIFY_ASSIGN, mCurrentRowH));
		else
			MsiUtilities::Verify(MsiViewModify(mViewHandle, MSIMODIFY_UPDATE, mCurrentRowH));
	}
	else
	{
		MsiUtilities::LogError(L"The row provided does not have a valid format.");
	}
}

void MsiView::DeleteCurrRow()
{
	MsiUtilities::Verify(MsiViewModify(mViewHandle, MSIMODIFY_DELETE, mCurrentRowH));
	MsiUtilities::Verify(MsiCloseHandle(mCurrentRowH));
	mCurrentRowH = NULL;
}

std::vector<MsiRow> MsiView::GetAllRows()
{
	std::vector<MsiRow> rows;
	auto currPos = mCurrPos;
	Reset();
#pragma warning(disable : 4127) //conditional expression is constant
	while (Fetch())
	{
		rows.push_back(std::move(mCurrentRow));
	}
#pragma warning(default : 4127)

	//restore position
	GoToRow(currPos);

	return rows;
}

unsigned int MsiView::GetCurrRowIndex()
{
	return mCurrPos;
}

unsigned int MsiView::GetRowsCount()
{
	//everytime a fetch is done, mCurrPos is increased
	while (Fetch()){}

	return mCurrPos;
}

void MsiView::GoToRow(unsigned int pIndex)
{
	Reset();
	//make a fetch untill we reached the end of the view or the index reaches 0
	while (pIndex-- && Fetch()){}
}

//  protected
MsiView::MsiView()
	: mEndOfViewFlag(false), mCurrentRowH(NULL), mCurrPos(0), mViewHandle(NULL)
{

}

void MsiView::Execute(const MsiDB& pDb, const MsiQuery& pQuery)
{
	CloseHandles();

	MsiUtilities::Verify(MsiDatabaseOpenView(pDb.GetHandle(), pQuery.GetQuery().c_str(), &mViewHandle));
	MsiUtilities::Verify(MsiViewExecute(mViewHandle, NULL));
}
//--protected

void MsiView::UpdateRow()
{
	for (unsigned int index = 1; index <= mMeta.ColCount(); index++)
	{
		//the values are get directly because it's sure they will not throw
		switch (mMeta[index].Type())
		{
		case MsiDataType::INTEGER:
			MsiUtilities::Verify(MsiRecordSetInteger(mCurrentRowH, index, mCurrentRow[index].Int().Get()));
			break;
		case MsiDataType::STRING:
			MsiUtilities::Verify(MsiRecordSetString(mCurrentRowH, index, (mCurrentRow[index].String().Get()).c_str()));
			break;
		case MsiDataType::STREAM:
			SetStreamCell(index);
			break;
		}
	}
}

void MsiView::SetStreamCell(unsigned int pCellIndex)
{
	auto stream = mCurrentRow[pCellIndex].Stream();
	if (stream && stream.Get() != MsiCell::NullStream)
	{
#pragma warning(disable : 4996) //fopen and tmpnam are unsafe
		//open a temporary file, write the stream in it and send it to the cell
		auto tmpNameStr = std::string(tmpnam(NULL)).substr(1); //remove the '\' from the beginning
		auto tmpName = tmpNameStr.c_str();
		FILE* tmpFile = fopen(tmpName, "wb+");
		if (tmpFile)
		{
			fwrite((void*)stream.Get().data(), sizeof(stream.Get()[0]), stream.Get().size(), tmpFile);
			fclose(tmpFile);

			auto mby = MsiUtilities::Verify(MsiRecordSetStreamA(mCurrentRowH, pCellIndex, tmpName), false);
			remove(tmpName);
		}
		else
		{
			MsiUtilities::LogError(L"Cannot create tmp file in order to write the stream");
		}
#pragma warning(default : 4996)
	}
}


bool MsiView::IsValid(const MsiRow& pRow)
{
	//check the correctness of the row provided
	for (unsigned int index = 1; index <= mMeta.ColCount(); index++)
	{
		if (!mMeta[index].IsNullable() && pRow[index].Type() != mMeta[index].Type())
		{
			return false;
		}
	}
	return true;
}

void MsiView::FillMeta()
{
	PMSIHANDLE colTypesH = 0;
	MsiUtilities::Verify(MsiViewGetColumnInfo(mViewHandle, MSICOLINFO_TYPES, &colTypesH));
	
	PMSIHANDLE colNamesH = 0;
	MsiUtilities::Verify(MsiViewGetColumnInfo(mViewHandle, MSICOLINFO_NAMES, &colNamesH));

	unsigned int colCount = MsiRecordGetFieldCount(colTypesH);
	if (colCount == -1 || colCount == 0xffffff)
	{
		MsiUtilities::CriticalError(L"The column count is incorrect!");
	}

	for (unsigned int index = 1; index <= colCount; index++)
	{
		MsiColInfo colInfo;
		//set the columns names
		auto nameMby = GetRecordString(colNamesH, index);
		if (nameMby)
			colInfo.Name(nameMby.Get());
		else
			MsiUtilities::CriticalError(L"Could not fill columns names in Metadata! " + nameMby.GetError().Str());

		//set the columns types
		auto typeMby = GetRecordString(colTypesH, index);
		if (typeMby)
			FillType(colInfo, typeMby.Get());
		else
			MsiUtilities::CriticalError(L"Could not fill columns names in Metadata! " + nameMby.GetError().Str());
		
		mMeta.AddColInfo(std::move(colInfo));
	}
}

void MsiView::FillType(MsiColInfo& pColInfo, const std::wstring& pType)
{
	wchar_t typeId = pType[0];
	unsigned int typeSize = std::stoul(pType.substr(1));

	if (std::iswlower(typeId))
		pColInfo.IsNullable(false);

	typeId = std::towlower(typeId);

	if (typeId == 'l')
		pColInfo.IsLocalizable(true);

	switch (typeId)
	{
	case 'i':
	case 'j':
		pColInfo.Type(MsiDataType::INTEGER);
		break;
	case 'v':
	case 'O':
		pColInfo.Type(MsiDataType::STREAM);
		break;
	case 's':
	case 'l':
	case 'g':
		pColInfo.Type(MsiDataType::STRING);
		pColInfo.Size(typeSize);
		break;
	}
}

Maybe<std::wstring> MsiView::GetRecordString(MSIHANDLE pHandle, unsigned int pField) const
{
	if (unsigned long dataSize = MsiRecordDataSize(pHandle, pField))
	{
		//add one for the NULL
		dataSize++;
		wchar_t* str = new wchar_t[dataSize];

		auto mby = MsiUtilities::Verify(MsiRecordGetString(pHandle, pField, str, &dataSize), false);
		if (!mby)
		{
			return mby.GetError();
		}

		std::wstring returnVal(str, dataSize);
		delete[]str;
		return returnVal;
	}
	else
	{
		//MsiUtilities::LogInfo(L"GetRecordString returned an empty string!");
		return L"";
	}
}

Maybe<std::vector<char>> MsiView::GetRecordStream(MSIHANDLE pHandle, unsigned int pField) const
{
	unsigned long dataSize = MsiRecordDataSize(pHandle, pField);
	std::vector<char> buffer(dataSize);

	if (dataSize)
	{
		auto mby = MsiUtilities::Verify(MsiRecordReadStream(pHandle, pField, buffer.data(), &dataSize));
		if(!mby)
		{
			return mby.GetError();
		}
	}
	else
	{
		MsiUtilities::LogInfo(L"GetRecordStream returned an empty stream!");
	}

	return buffer;
}


void MsiView::CloseHandles()
{
	if (mViewHandle)
	{
		MsiUtilities::Verify(MsiViewClose(mViewHandle), false);
		MsiUtilities::Verify(MsiCloseHandle(mViewHandle), false);
		mViewHandle = NULL;
	}

	if (mCurrentRowH)
	{
		MsiUtilities::Verify(MsiCloseHandle(mCurrentRowH), false);
		mCurrentRowH = NULL;
	}
}