#include "MsiCommonFilters.h"
#include "MsiCells.h"


MsiFetchFilters::CallableFilter MsiCommonFilters::MatchPrimaryKey(const MsiCells& pCheckPk)
{
	return [=](const MsiMeta& meta, const MsiRow& row)
	{
		unsigned int pCheckPkIndex = 1;
		auto pkIndices = meta.GetPKIndices();
		if (pCheckPk.Size() != pkIndices.size())
			return false;

		for (unsigned int pkIndex : pkIndices)
			if (row[pkIndex] != pCheckPk[pCheckPkIndex++])
				return false;

		return true;
	};
}

MsiFetchFilters::CallableFilter MsiCommonFilters::MatchCells(const std::unordered_map<unsigned int, MsiCell>& pCells)
{
	return [=](const MsiMeta&, const MsiRow& row)
	{
		for (const auto cell : pCells)
			if (row[cell.first] != cell.second)
				return false;

		return true;
	};
}

MsiFetchFilters::CallableFilter MsiCommonFilters::MatchCells(const std::unordered_map<std::wstring, MsiCell>& pCells)
{
	return [=](const MsiMeta& meta, const MsiRow& row)
	{
		for (const auto cell : pCells)
		{
			auto index = meta.IndexOf(cell.first);
			if (!index)
			{
				MsiUtilities::LogInfo(L"The cell " + cell.first + L" doesn't exists: " + index.GetError().GetMessage());
				return false;
			}

			if (row[index.Get()] != cell.second)
				return false;

		}
		return true;
	};
}
