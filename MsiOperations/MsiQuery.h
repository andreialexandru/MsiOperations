#pragma once
#include "MsiUtilities.h"
#include <string>
#include <vector>

/** @file */
class MsiMeta;
class MsiColInfo;
class MsiColOperations;
class MsiTable;
class MsiColOp;

/**
*Wrapper class over a query string, with helper static functions.
*
*This class is a wrapper over a std::wstring representing an SQL query.
*It also provides static functions to build MsiQuery objects for various situations.
*@see MsiView
*/
class MSI_EXPORT MsiQuery
{
public:
	/**
	*Class Constructor.
	*
	*It copies the query string received into a member for later access.
	*@param pQueryStr Query string of type "SELECT * FROM TableName".
	*/
	MsiQuery(std::wstring pQueryStr);
	
	/**
	*Setter for the query string.
	*
	*@param pQueryStr Query string of type "SELECT * FROM TableName".
	*/
	void SetString(std::wstring pQueryStr);

	/**
	*Getter for the query string.
	*
	*@return Query string of type "SELECT * FROM TableName".
	*/
	const std::wstring& GetQuery() const;

	/**
	*Specify the column to order by.
	*
	*@param pColName The column to order by.
	*@remark Calling this function multiple times, will accumulate the columns to order by.
	*For example:
	*@code
	*	OrderBy(L"abc");
	*	OrderBy(L"def");
	*@endcode
	*Will result in a sql command similar to this:
	*@code
	*	ORDER BY abc, def
	*@endcode
	*/
	void OrderBy(std::wstring pColName);

	/**
	*Specify the columns to order by.
	*
	*@param pColNames The columns to order by.
	*@remark Calling this function multiple times, will accumulate the columns to order by.
	*For example:
	*@code
	*	OrderBy(std::vector<std::wstring>{L"abc", L"def"});
	*	OrderBy(std::vector<std::wstring>{L"ghi", L"jkl"});
	*@endcode
	*Will result in a sql command similar to this:
	*@code
	*	ORDER BY abc, def, ghi, jkl
	*@endcode
	*/
	void OrderBy(std::vector<std::wstring> pColNames);

	/**
	*Helper function used to construct a MsiQuery that creates a table in the Msi Database.
	*
	*@param pTableName The name of the table we wish to create.
	*@param pMeta the schema of the table we wish to create.
	*@return A query object containing the query to create a table.
	*@see MsiMeta
	*/
	static MsiQuery CreateTableQuery(const std::wstring& pTableName, const MsiMeta& pMeta);

	/**
	*Helper function used to construct a MsiQuery that adds a column to a table in the Msi Database.
	*
	*@param pTableName The name of the table in which we want to add the column.
	*@param pColInfo The information of the column we want to add.
	*@return A query object containing the query to add a column.
	*@see MsiColInfo
	*/
	static MsiQuery AddColumnQuery(const std::wstring& pTableName, const MsiColInfo& pColInfo);
	
	/**
	*Helper function used to construct a MsiQuery that selects the rows from one or several
	*tables based on a condition.
	*
	*@param pTables The tables that we want to select from.
	*@param pOp [Might be empty ("")] The operation that will be executed when selecting (a SQL WHERE clause).
	*@param pCols [default = {}] The columns we want to select. If empty, all the columns will be selected.
	*@see MsiColOperations
	*/
	static MsiQuery SelectQuery(
		const std::vector<std::wstring>& pTables,
		const std::wstring& pOp,
		const std::vector<std::wstring>& pCols = {});

	/**
	*Helper function used to construct a MsiQuery that selects all the rows from one or several
	*tables.
	*
	*@param pTables The tables that we want to select from.
	*@param pCols [default = {}] The columns we want to select. If empty, all the columns will be selected.
	*/
	static MsiQuery SelectQuery(
		const std::vector<std::reference_wrapper<MsiTable>>& pTables,
		const std::vector<MsiColOp>& pCols = {});

	/**
	*Helper function used to construct a MsiQuery that selects the rows from one or several
	*tables based on a condition.
	*
	*@param pTables The tables that we want to select from.
	*@param pOp The operation that will be executed when selecting (a SQL WHERE clause).
	*@param pCols [default = {}] The columns we want to select. If empty, all the columns will be selected.
	*@see MsiColOperations
	*/
	static MsiQuery SelectQuery(
		const std::vector<std::reference_wrapper<MsiTable>>& pTables,
		const MsiColOperations& pOp,
		const std::vector<MsiColOp>& pCols = {});

private:

	void UpdateQuery();

	std::wstring mQueryStr;
	std::vector<std::wstring> mOrderBy;
};

