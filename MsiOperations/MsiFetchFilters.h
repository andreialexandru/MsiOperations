#pragma once
#include "MsiCells.h"
#include "MsiMeta.h"
#include <functional>

/**@file*/

/**
*Represents a group of filters over the results of a query.
*
*This function holds a collection of functions that act like filters over the
*results of a query (a MsiView). The filters must be callable objects(lambda, function object,
*function pointer) that receive as arguments a MsiMeta and a ::MsiRow and returns a boolean value
*that informs the MsiView::Fetch() if the row meets the filter's conditions or not. MsiView::Fetch() will
*iterate through all the filters received and check if the fetched row matches them. If must match all of
*the filters in order to be set as current row in the MsiView.
*@see MsiCommonFilters for usage examples
*@see MsiView::Fetch()
*/
class MSI_EXPORT MsiFetchFilters
{
public:
	/**
	*Naming the callable object used as a filter for a easier use. 
	*/
	using CallableFilter = std::function < bool(const MsiMeta&, const MsiRow&) >;

	/**
	*Class constructor.
	*
	*Default constructor used in declarations without initialization and as 
	*a default argument for MsiView::Fetch().
	*
	*@see MsiFetchFilters(CallableFilter pFilter, Args&&... pFilters)
	*/
	MsiFetchFilters(){}

	/**
	*Class constructor.
	*
	*It receives a variable number of MsiFetchFilters::CallableFilter (callable objects) and holds them to be used later
	*to filter the results of MsiView::Fetch().
	*
	*@remark First argument is not an universal reference so it doesn't overwrite the copy and move constructors.
	*/
	template<typename ...Args>
	MsiFetchFilters(CallableFilter pFilter, Args&&... pFilters);

	/**
	*Adds cells to an existing MsiFetchFilters collection.
	*
	*It receives a variable number of MsiFetchFilters::CallableFilter (callable objects) and holds them to be used later
	*to filter the results of MsiView::Fetch().
	*
	*@see MsiFetchFilters(CallableFilter pFilter, Args&&... pFilters)
	*/
	template<typename ...Args>
	void AddFilters(CallableFilter pFilter, Args&&... pFilters);

	/**
	*Getter for the collection of filters.
	*
	*@return A standard vector of MsiFetchFilters::CallableFilter.
	*/
	const std::vector<CallableFilter>& GetFilters() const { return mFilters; }
private:
	//Needed by the templated AddFilters for the recursive call.
	void AddFilters(){}

	std::vector<CallableFilter> mFilters;
};

#include "MsiFetchFilters.hpp"