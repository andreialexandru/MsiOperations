#include "MsiUtilities.h"
#include "Maybe.h"
#include <Windows.h>
#include <Msi.h>
#include <MsiQuery.h>
#include <iostream>

void MsiUtilities::LogMsi(unsigned int pInstallHandle, const std::wstring& pMessage)
{
	PMSIHANDLE recordH = MsiCreateRecord(1);
	if(Verify(MsiRecordSetString(recordH, 1, pMessage.c_str()), false))
		MsiProcessMessage(pInstallHandle, INSTALLMESSAGE_INFO, recordH);
}

void MsiUtilities::LogInfo(const std::wstring& pMessage)
{
	std::wstring info = L"[INFO]" + pMessage + L"\n";
	std::wclog << info << std::endl;
	OutputDebugString(info.c_str());
}

void MsiUtilities::LogError(const std::wstring& pMessage)
{
	std::wstring error = L"[ERROR]" + pMessage + L"\n";
	std::wcerr << error << std::endl;
	OutputDebugString(error.c_str());
}

void MsiUtilities::CriticalError(const std::wstring& pMessage)
{
	LogError(pMessage);
	throw Exception(ERROR_FUNCTION_FAILED, pMessage);
}

void MsiUtilities::CriticalError(const Exception& pException)
{
	LogError(pException.GetMessage());
	throw pException;
}


Maybe<void> MsiUtilities::Verify(unsigned int pErrorCode, bool pCritical)
{
	std::wstring errorMsg;

	PMSIHANDLE hLastErrorRec = MsiGetLastErrorRecord();
	if (pErrorCode != ERROR_SUCCESS && hLastErrorRec)
	{
		unsigned long errorLen = 0;
		UINT uiStatus = MsiFormatRecord(NULL,
			hLastErrorRec,
			TEXT(""),
			&errorLen);

		if (ERROR_MORE_DATA == uiStatus)
		{
			// returned size does not include null terminator.
			errorLen++;

			wchar_t* extendedError = new wchar_t[errorLen];
			uiStatus = MsiFormatRecord(NULL,
				hLastErrorRec,
				extendedError,
				&errorLen);
			if (ERROR_SUCCESS == uiStatus)
			{
				errorMsg = extendedError;
			}

			delete[] extendedError;
			extendedError = nullptr;
		}

		if (pCritical)
			throw Exception(pErrorCode, errorMsg);
		else
			return Maybe<void>(Exception(pErrorCode, errorMsg));
	}
	
	return Maybe<void>();
}

Maybe<bool> MsiUtilities::ConditionEvaluator(MSIHANDLE pInstallHandle, const std::wstring& pCondition)
{
	auto condition = MsiEvaluateCondition(pInstallHandle, pCondition.c_str());

	if (condition == MSICONDITION_FALSE)
		return false;
	
	if (condition == MSICONDITION_TRUE)
		return true;

	return Exception(condition, L"ConditionEvaluator failed!");
}

Maybe<std::wstring> MsiUtilities::FormatString(MSIHANDLE pInstallHandle, const std::wstring& pStr)
{
	PMSIHANDLE recordH = MsiCreateRecord(1);
	Verify(MsiRecordSetString(recordH, 0, L"[1]"), false);
	Verify(MsiRecordSetString(recordH, 1, pStr.c_str()), false);

	unsigned long stringSize = 0;

	auto res = MsiFormatRecord(pInstallHandle, recordH, L"", &stringSize);

	if (res != ERROR_MORE_DATA)
	{
		return Verify(res, false).GetError();
	}

	wchar_t* str = new wchar_t[++stringSize];

	auto mby = MsiUtilities::Verify(MsiFormatRecord(pInstallHandle, recordH, str, &stringSize), false);
	if (!mby)
	{
		return mby.GetError();
	}

	std::wstring returnVal(str, stringSize);
	delete[]str;
	return returnVal;
}