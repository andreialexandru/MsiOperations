#include "Utils.h"
#include <string>
#include <MsiTable.h>

bool OrderByTest(const std::wstring& pMsiPath)
{
	MsiDB msiDb(pMsiPath);

	MsiTable msiTable(msiDb, L"Control");
	msiTable.OrderBy(std::vector<std::wstring>{ L"Width", L"X" });

	PrintView(msiTable, 10);

	return true;
}
