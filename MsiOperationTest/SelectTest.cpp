#include "Utils.h"
#include <string>
#include <MsiTable.h>
#include <MsiColOperations.h>

bool SelectTest(const std::wstring& pMsiPath)
{
	MsiDB msiDb(pMsiPath);

	MsiTable fileT(msiDb, L"File");
	MsiTable componentT(msiDb, L"Component");

	MsiColOp f_comp(fileT, L"Component_");
	MsiColOp c_comp(componentT, L"Component");
	MsiColOp c_dir(componentT, L"Directory_");

	MsiQuery query = MsiQuery::SelectQuery(
		{ fileT, componentT },
		f_comp == c_comp && c_dir == L"bin",
		{ MsiColOp(fileT, L"File"), MsiColOp(componentT, L"ComponentId") }
	);
	
	PrintView(MsiView(msiDb, query));

	return true;
}