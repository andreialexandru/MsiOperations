#pragma once
#include "MsiCells.h"
#include "MsiMeta.h"
#include "MsiView.h"

extern void PrintRow(const MsiRow& row, const MsiMeta& meta);
extern void PrintView(MsiView& msiView, int max = 0);