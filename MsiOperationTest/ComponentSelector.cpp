#include "Maybe.h"
#include "MsiDB.h"
#include "MsiTable.h"
#include "MsiCommonFilters.h"
#include <string>
#include <iostream>

bool ComponentSelector(const std::wstring& pMsiPath, const std::wstring& pComponent)
{
	MsiDB msiDb(pMsiPath);

	MsiTable msiTable(msiDb, L"File");

	unsigned int found = 0;

	while (msiTable.Fetch(MsiCommonFilters::MatchCells({ { L"Component_", pComponent } })))
	{
		found++;

		auto index = msiTable.GetMeta().IndexOf(L"File");
		if (index)
		{
			auto fileName = msiTable.GetCurrentRow()[index.Get()].String();
			if (fileName)
			{
				std::wcout << fileName.Get() << " ";
			}
			else
			{
				MsiUtilities::LogInfo(L"Could not retrieve the file name from File table.");
				return false;
			}
		}
		else
		{
			MsiUtilities::LogInfo(L"Could not find the column 'File': " + index.GetError().GetMessage());
			return false;
		}
	}

	if (!found)
	{
		MsiUtilities::LogInfo(L"The component " + pComponent + L" was not found in the table File");
		return false;
	}

	return true;
}