
#include "MsiDB.h"
#include "MsiTable.h"
#include "MsiColOperations.h"
#include <string>

bool AddListBox(const std::wstring& pMsiPath)
{
	MsiDB msiDb(pMsiPath);

	const std::wstring dialog = L"WelcomeDlg";
	const std::wstring listName = L"MyList";

	//add combo to dialog
	MsiTable controlT(msiDb, L"Control");
	controlT.UpdateCurrRow({ dialog, listName, L"ListBox", 250, 125, 100, 100, 65539, L"LIST", MsiCell::NullString, MsiCell::NullString, MsiCell::NullString });

	//create querry to get all the id's of the controlls in the dialog
	MsiTable dialogT(msiDb, L"Dialog");
	MsiColOp dlg_d(dialogT, L"Dialog");
	MsiColOp ctrlFirst_d(dialogT, L"Control_First");

	MsiColOp dlg_c(controlT, L"Dialog_");
	MsiColOp ctrl_c(controlT, L"Control");
	MsiColOp ctrlNext_c(controlT, L"Control_Next");

	MsiQuery queryFirst = MsiQuery::SelectQuery(
		{ dialogT, controlT },
		dlg_d == dialog && dlg_c == dlg_d && ctrl_c == ctrlFirst_d,
		{ ctrl_c, ctrlNext_c }
	);

	MsiView viewFirst(msiDb, queryFirst);
	viewFirst.Fetch();
	auto firstRow = viewFirst.GetCurrentRow();

	std::wstring ctrlFirst = firstRow[1].String().Get();
	std::wstring prevNext = firstRow[2].String().Get();
	

	//insert first element
	auto index = 1;
	MsiTable coboT(msiDb, L"ListBox");
	coboT.UpdateCurrRow({ L"LIST", index++, ctrlFirst, ctrlFirst });

	//insert the rest
	while (prevNext != ctrlFirst)
	{
		MsiQuery queryNext = MsiQuery::SelectQuery(
		{ controlT },
		dlg_c == dialog && ctrl_c == prevNext,
		{ ctrl_c, ctrlNext_c }
		);

		MsiView view(msiDb, queryNext);

		auto ctrlIndex = view.GetMeta().IndexOf(L"Control");
		auto ctrlNextIndex = view.GetMeta().IndexOf(L"Control_Next");

		if (!ctrlIndex || !ctrlNextIndex)
		{
			MsiUtilities::LogInfo(L"Could not find column Control");
			return false;
		}

		view.Fetch();
		const auto& currentRow = view.GetCurrentRow();
		coboT.UpdateCurrRow({ L"LIST", index++, currentRow[ctrlIndex.Get()], currentRow[ctrlIndex.Get()] });
		prevNext = currentRow[ctrlNextIndex.Get()].String().Get();
	}
	return true;
}